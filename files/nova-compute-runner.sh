#!/bin/bash
##
## Copyright (c) 2021 The Yaook Authors.
##
## This file is part of Yaook.
## See https://yaook.cloud for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##

set -euxo pipefail

groupadd --gid ${CINDER_GID} cinder && usermod -aG cinder nova

echo "[DEFAULT]" > /tmp/ipoverwrite.conf
echo "my_ip = ${NODE_IP}" >> /tmp/ipoverwrite.conf
echo "[libvirt]" >> /tmp/ipoverwrite.conf
echo "live_migration_inbound_addr = ${NODE_IP}" >> /tmp/ipoverwrite.conf
echo "[vnc]" >> /tmp/ipoverwrite.conf
echo "server_proxyclient_address = ${NODE_IP}" >> /tmp/ipoverwrite.conf

cat /tmp/ipoverwrite.conf

mkdir -p /var/lib/nova/instances
chown nova:nova /var/lib/nova/instances

echo "Waiting for credential configuration"

sleep 10

# The setpriv version available on CentOS/AlmaLinux currently does not support specifying users und groups as names
NOVA_UID=$(id -u nova)
NOVA_GID=$(getent group nova | cut -f 3 -d :)
LIBVIRT_GID=$(getent group libvirt | cut -f 3 -d :)
exec setpriv --reuid ${NOVA_UID} --regid ${NOVA_GID} --groups ${NOVA_UID},${CINDER_GID},${LIBVIRT_GID} -- nova-compute --config-file /etc/nova/nova.conf --config-file /tmp/ipoverwrite.conf
