#!/bin/bash
##
## Copyright (c) 2021 The Yaook Authors.
##
## This file is part of Yaook.
## See https://yaook.cloud for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##
set -euo pipefail

. /keyutils.lib.sh

# first things first: generate the keys
# note that generate_key will mercilessly overwrite existing keys
echo "generating host keys ..."
hostkeys="/etc/ssh/hostkeys/"
generate_key "$hostkeys/ed25519" "ed25519"

echo "generating user keys ..."
userkeys="/mnt/client-ssh-home"
generate_key "$userkeys/id_ed25519" "ed25519"

echo "key generation complete!"

# collect things to talk to the k8s API in variables we use later on
kubernetes_api="https://kubernetes.default.svc:${KUBERNETES_SERVICE_PORT_HTTPS}"
auth_header="Authorization: Bearer $(cat /var/run/secrets/kubernetes.io/serviceaccount/token)"
content_type_header="Content-Type: application/json-patch+json"
cacert=/var/run/secrets/kubernetes.io/serviceaccount/ca.crt
namespace="$(cat /var/run/secrets/kubernetes.io/serviceaccount/namespace)"
sshidentities_endpoint="$kubernetes_api/apis/compute.yaook.cloud/v1/namespaces/$namespace/sshidentities"

scratchspace="$(mktemp -d)"
all_hostkeys="$scratchspace/hostkeys"
cut -d' ' -f1-2 "$hostkeys"/*.pub > "$all_hostkeys"
all_novakeys="$scratchspace/novakeys"
cut -d' ' -f1-2 "$userkeys"/*.pub > "$all_novakeys"
sshidentity_patch="$scratchspace/patch.json"

printf 'collected %d host and %d user keys\n' "$(wc -l < "$all_hostkeys")" "$(wc -l < "$all_novakeys")"

# compile the sshidentity patch. thanks jq!
jq -n '[{"op": "replace", "path": "/status", "value": {"keys": {"host": ($hostkeys / "\n"), "user": ($novakeys / "\n")}}}]' --arg namespace "$namespace" --arg name "$YAOOK_NOVA_COMPUTE_PUBLIC_KEYS" --arg hostkeys "$(cat "$all_hostkeys")" --arg novakeys "$(cat "$all_novakeys")" > "$sshidentity_patch"

response="$scratchspace/response"

while true; do
    # and now, retry forever to update the secret until we eventually succeed.
    status_code="$(curl \
        -s \
        -o "$response" \
        --cacert "$cacert" \
        -X PATCH \
        -w '%{http_code}' \
        -H "$auth_header" \
        -H "$content_type_header" \
        --data-binary @"$sshidentity_patch" \
        "$sshidentities_endpoint/$YAOOK_NOVA_COMPUTE_PUBLIC_KEYS/status")"
    is_ok=0
    case "$status_code" in
        2*)
            is_ok=1
            ;;
        4*)
            echo "Config map update failed fatally ($status_code)!" >&2
            cat "$response" >&2
            echo
            cat "$sshidentity_patch" >&2
            exit 1
            ;;
        5*)
            echo "Server error, retrying in a moment" >&2
            cat "$response" >&2
            ;;
        *)
            echo "unexpected response code: $status_code" >&2
            cat "$response" >&2
            echo "retrying in a moment" >&2
            ;;
    esac
    if [ "$is_ok" = '1' ]; then
        break
    fi
    sleep 15
done

echo "keys published successfully!"

echo "waiting for host key to appear in known_hosts"

generated_hostkey=$(cat "$all_hostkeys")

TIMEOUT=120
t=$TIMEOUT
found=0

while (( t > 0 )); do
    if grep -q "${generated_hostkey}" /publickeys/known_hosts; then
        echo "ok, found host key in known_hosts"
        found=1
        break
    fi

    echo -n "."
    sleep 1
    (( t -= 1 ))
done || true

echo

if [ "$found" = "0" ]; then
    echo "Warning: generated host key not found in known_hosts file!"
fi

echo "giving the host keys some more time to be distributed"
sleep 20

rm -rf -- "$scratchspace"
