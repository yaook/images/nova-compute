#!/bin/bash
##
## Copyright (c) 2021 The Yaook Authors.
##
## This file is part of Yaook.
## See https://yaook.cloud for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##

set -euxo pipefail

groupadd --gid ${CINDER_GID} cinder && usermod -aG cinder nova

if [ -e /dev/kvm ]; then
    novakvmgid=$(stat /dev/kvm | jc --stat | jq .[].gid)
    echo "GID for /dev/kvm on Hypervisor is $novakvmgid"

    echo "creating novakvm group with GID $novakvmgid by appending 'novakvm:x:$novakvmgid:nova' to /etc/group"
    echo "novakvm:x:$novakvmgid:nova" >> /etc/group
fi

# assign the swtpm-localca directory to nova:libvirt otherwise we fail due to permission issues (swtpm process also runs via nova:libvirt)
if [ -d /var/lib/swtpm-localca ]; then
    chown nova:libvirt /var/lib/swtpm-localca
fi

# assign the sev device (for encrypted memory) nova:libvirt otherwise we fail due to permission issues (nova-compute needs to access this device)
if [ -c /dev/sev ]; then
    chown 2500008:2500008 /dev/sev
fi

rm -f /var/run/libvirtd.pid

# As our approaches differ for the cgroup version (v1 cgroupfs, v2 systemd) we need to validate which version is currently running
CGROUPVERSION=v2

for c in $(mount | grep cgroup | jc --mount | jq .[].type); do
    if [[ "$c" ==  '"cgroup"' ]]; then
        CGROUPVERSION=v1;
    fi
done

#Setup Cgroups to use when breaking out of Kubernetes defined groups and to escape the subreaper of containerd
if [[ $CGROUPVERSION == v1 ]]; then
    CGROUPS=""
    for CGROUP in cpu memory rdma hugetlb; do
        if [ -d /sys/fs/cgroup/${CGROUP} ]; then
            CGROUPS+="${CGROUP},"
        fi
    done
    cgcreate -g ${CGROUPS%,}:/libvirtd

    # Since /var/run is hostPath-mounted (which we should change eventually…), we
    # have to remove the socket manually. We do NOT remove the other runtime state
    # though since that is supposed to survive a libvirtd restart.
    # #learnedthatthehardway
    rm -rf /var/run/libvirt/libvirt-{admin-sock,sock-ro,sock}
    cgexec -g ${CGROUPS%,}:/libvirtd systemd-run --scope --slice=system libvirtd --listen &
    MAIN_PROCESS_PID=$!
elif [[ $CGROUPVERSION == v2 ]]; then
    if ! gdbus call --system --dest org.freedesktop.systemd1 --object-path /org/freedesktop/systemd1 --method org.freedesktop.systemd1.Manager.GetUnit libvirt.slice 2>&1 > /dev/null; then
        gdbus call --system --dest org.freedesktop.systemd1 --object-path /org/freedesktop/systemd1 --method org.freedesktop.systemd1.Manager.StartTransientUnit libvirt.slice replace "[]" "[]"
    fi
    rm -rf /var/run/libvirt/libvirt-{admin-sock,sock-ro,sock}
    # as we escape the container boundaries also with libvirtd, we need to make
    # sure that libvirtd is killed before we try to start it
    if [[ $(pidof libvirtd) ]]; then
      kill $(pidof libvirtd)
    fi
    nsenter --cgroup=/proc/1/ns/cgroup systemd-run --scope --slice=libvirt libvirtd --listen &
    MAIN_PROCESS_PID=$!
else
    echo "cgroup could not be determined. Exiting..."
    exit 1
fi

TIMEOUT=30
while [ ! -e /var/run/libvirt/libvirt-sock ]; do
    if [[ ${TIMEOUT} -gt 0 ]]; then
      let TIMEOUT-=1
      echo "Waiting for libvirt socket to come up"
      sleep 1
    else
      echo "ERROR: libvirt did not start in time (socket missing)"
      exit 1
    fi
done

if [ -f /etc/ceph/idmap ]; then
  echo "Setting up ceph secrets"
  for i in "$(cat /etc/ceph/idmap)"; do
    uuid="$(echo "$i" | cut -d \; -f 1)"
    filename="$(echo "$i" | cut -d \; -f 2)"
    cat > /tmp/secret.xml <<EOF
<secret ephemeral='no' private='no'>
<uuid>${uuid}</uuid>
<usage type='ceph'>
    <name>${filename} secret</name>
</usage>
</secret>
EOF
    virsh secret-define --file /tmp/secret.xml
    set +x
    virsh secret-set-value --secret "${uuid}" --base64 "$(cat "${filename}")"
    set -x
    rm -rf /tmp/secret.xml
  done
  echo "Ceph setup finished"
else
  echo "idmap is no file. Skipping ceph setup."
fi

echo "All ready, now joining libvirt"
wait $MAIN_PROCESS_PID
