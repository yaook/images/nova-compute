##
## Copyright (c) 2021 The Yaook Authors.
##
## This file is part of Yaook.
## See https://yaook.cloud for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##

ARG release=2024.1
ARG branch=$release
ARG libvirt_version=10.5.0
ARG qemu_version=9.0.0
ARG swtpm_version=0.9
ARG nova_uid=2500008
ARG nova_gid=2500008
ARG libvirt_gid=2500009

ARG LANG=C.UTF-8
ARG DEBIAN_FRONTEND=noninteractive

ARG CINDER_GID=2500003

ARG BUILD_PACKAGES="build-essential ca-certificates curl gpg gpg-agent libxml2-utils meson ninja-build patch pkg-config python3-venv"
ARG PYTHON_BUILD_PACKAGES="build-essential git pkg-config python3-dev"
ARG QEMU_DEV_PACKAGES="libaio-dev libbluetooth-dev libbrlapi-dev libbz2-dev libcap-ng-dev libcapstone-dev libcurl4-gnutls-dev libfdt-dev libglib2.0-dev libibverbs-dev libjpeg8-dev libspice-protocol-dev libspice-server-dev liblzo2-dev libncurses5-dev libnuma-dev librbd-dev librdmacm-dev libsasl2-dev libsdl2-dev libseccomp-dev libsnappy-dev libssh-dev libvde-dev libvdeplug-dev libvte-2.91-dev policykit-1 valgrind xfslibs-dev"
ARG QEMU_REQUIRED_PACKAGES="libbrlapi0.8 libcapstone4 libfdt-dev libglib2.0-bin liblzo2-2 libpcap0.8 libpciaccess0 libsdl2-2.0-0 libspice-server1 libvdeplug2 libvte-2.91-0 libxml2 libyajl2"
ARG LIBVIRT_DEV_PACKAGES="gnutls-dev libapparmor-dev libaudit-dev libblkid-dev libcurl4-gnutls-dev libglib2.0-dev libnl-3-dev libnl-route-3-dev libnuma-dev libpcap-dev libpciaccess-dev librados-dev librbd-dev libsanlock-dev libsasl2-dev libudev-dev libxml2-dev libxml2-utils libyajl-dev numad python3-docutils systemtap-sdt-dev xsltproc"
ARG QEMU_PATCHES="0005-qemu-9.0-introduce-add-host-physical-bit-machine-type.patch 0006-virtio-gpu-fix-migrations.patch cve-2024-4467.patch"

ARG DESTDIR=/build

FROM ubuntu:22.04@sha256:0e5e4a57c2499249aafc3b40fcd541e9a456aab7296681a3994d631587203f97 AS builder

ARG libvirt_version
ARG qemu_version

ARG LANG
ARG DEBIAN_FRONTEND

ARG BUILD_PACKAGES
ARG QEMU_DEV_PACKAGES
ARG LIBVIRT_DEV_PACKAGES
ARG QEMU_PATCHES

ARG DESTDIR

COPY files/libvirt_signature_key.asc files/qemu_signature_key.asc /tmp/
COPY files/*.patch patches-qemu/ /patches/

RUN set -eux ; \
    apt-get update; \
    apt-get install -y --no-install-recommends \
        ${BUILD_PACKAGES} \
        ${QEMU_DEV_PACKAGES} \
        ${LIBVIRT_DEV_PACKAGES} \
        dmidecode \
        genisoimage \
        iproute2 \
        kmod \
        nfs-common \
        openvswitch-switch

RUN set -eux ; \
    curl -O "https://download.qemu.org/qemu-${qemu_version}.tar.xz"; \
    curl -O "https://download.qemu.org/qemu-${qemu_version}.tar.xz.sig"; \
    gpg --import /tmp/qemu_signature_key.asc; \
    gpg --verify qemu-${qemu_version}.tar.xz.sig; \
    tar xJf "qemu-${qemu_version}.tar.xz"; \
    for patch in ${QEMU_PATCHES}; \
        do patch -p1 -i /patches/${patch} -d "/qemu-${qemu_version}"; \
    done; \
    curl -O "https://download.libvirt.org/libvirt-${libvirt_version}.tar.xz"; \
    curl -O "https://download.libvirt.org/libvirt-${libvirt_version}.tar.xz.asc"; \
    gpg --import /tmp/libvirt_signature_key.asc; \
    gpg --verify libvirt-${libvirt_version}.tar.xz.asc; \
    tar -xf "libvirt-${libvirt_version}.tar.xz"

RUN set -eux ; \
    cd "/qemu-${qemu_version}"; \
    ./configure \
        --disable-docs \
        --disable-download \
        --disable-selinux \
        --disable-sndio \
        --disable-xen \
        --enable-lto \
        --enable-spice \
        --extra-cflags=-O2 \
        --firmwarepath=/usr/share/qemu:/usr/share/seabios:/usr/lib/ipxe/qemu \
        --libexecdir=/usr/lib/qemu \
        --prefix=/usr \
        --sysconfdir=/etc \
        --target-list="x86_64-softmmu"; \
    make -j8; \
    make install; \
    find ${DESTDIR}/ -empty -type d -print -delete

RUN set -eux ; \
    cd "/libvirt-${libvirt_version}"; \
    meson build \
        -Dapparmor=auto \
        -Daudit=enabled \
        -Dbindir=/usr/bin \
        -Dcapng=enabled \
        -Ddatadir=/usr/share \
        -Ddriver_bhyve=disabled \
        -Ddriver_esx=disabled \
        -Ddriver_hyperv=disabled \
        -Ddriver_interface=enabled \
        -Ddriver_libvirtd=enabled \
        -Ddriver_libxl=disabled \
        -Ddriver_lxc=disabled \
        -Ddriver_network=disabled \
        -Ddriver_openvz=disabled \
        -Ddriver_qemu=enabled \
        -Ddriver_vbox=disabled \
        -Ddriver_vmware=disabled \
        -Ddriver_vz=disabled \
        -Ddtrace=enabled \
        -Dexpensive_tests=enabled \
        -Dfirewalld=disabled \
        -Dfuse=disabled \
        -Dincludedir=/usr/include \
        -Dinfodir=/usr/share/info \
        -Dinit_script=systemd \
        -Dlibdir=/usr/lib \
        -Dlibexecdir=/usr/libexec \
        -Dlibpcap=enabled \
        -Dlibssh=disabled \
        -Dlocalstatedir=/var \
        -Dlogin_shell=disabled \
        -Dmandir=/usr/share/man \
        -Dnetcf=disabled \
        -Dnss=disabled \
        -Dnumactl=enabled \
        -Dnumad=enabled \
        -Dpm_utils=disabled \
        -Dprefix=/usr \
        -Dqemu_user=root \
        -Dqemu_group=root \
        -Drunstatedir=/run \
        -Dsanlock=enabled \
        -Dsbindir=/usr/sbin \
        -Dselinux=disabled \
        -Dsharedstatedir=/usr/libexec \
        -Dstorage_disk=disabled \
        -Dstorage_fs=enabled \
        -Dstorage_gluster=disabled \
        -Dstorage_iscsi=disabled \
        -Dstorage_iscsi_direct=disabled \
        -Dstorage_lvm=disabled \
        -Dstorage_mpath=disabled \
        -Dstorage_rbd=enabled \
        -Dstorage_scsi=disabled \
        -Dstorage_vstorage=disabled \
        -Dstorage_zfs=disabled \
        -Dsysconfdir=/etc \
        -Dtls_priority=@LIBVIRT,SYSTEM \
        -Dudev=enabled \
        -Dwerror=false \
        -Dwireshark_dissector=disabled \
        -Dyajl=enabled; \
    ninja -C build; \
    ninja -C build install; \
    find ${DESTDIR}/ -empty -type d -print -delete

# As we move now to selfbuild Qemu we truncate the filesize of certain
# roms to be compatible with the ubuntu roms used. Therefore we can
# guaranteee a smooth migration path.
# If these roms are not of the same size live-migration will not work.
RUN set -eux ; \
    truncate --size=524288 ${DESTDIR}/usr/share/qemu/efi-*

# Compile swtpm as the sources are too old
FROM ubuntu:22.04 AS swtpm-builder
ARG LANG
ARG DEBIAN_FRONTEND
ARG swtpm_version

RUN apt-get update && apt-get install --no-install-recommends -y \
        apt-transport-https \
        ca-certificates \
        git \
        make \
        build-essential \
        fakeroot \
        devscripts \
        equivs

WORKDIR /build
RUN git clone --depth 1 --branch stable-${swtpm_version} https://github.com/stefanberger/swtpm.git
WORKDIR /build/swtpm
RUN mk-build-deps --install --build-dep '--tool=apt-get --no-install-recommends -y' debian/control
RUN echo "libtpms0 libtpms" > ./debian/shlibs.local
RUN sed -i -e '/override_dh_auto_test/,+1d' ./debian/rules # the tests are flapping in ci
RUN DEB_BUILD_OPTIONS=nocheck debuild -us -uc

# Compile ovmf_4m_non-secureboot within a dedicated buildcontainer. To provide specific efivars we enable secure-boot (its just enabled in vars but not used).
# This configuration is the state of ubuntu/jammy package (later releases moved the default secure_boot to the secureboot ovmf)
FROM ubuntu:22.04 AS ovmf-builder
RUN apt update && apt install -y --no-install-recommends build-essential git uuid-dev iasl nasm apt-transport-https ca-certificates libgnutls30 python-is-python3 python3
WORKDIR /src
RUN git clone --depth 1 https://github.com/tianocore/edk2.git --branch edk2-stable202411 && \
    cd edk2 && \
    git submodule update --init
WORKDIR /src/edk2
RUN make -C BaseTools
ENV EDK_TOOLS_PATH=/src/edk2/BaseTools
ENV WORKSPACE=/src/edk2
ENV CONF_PATH=/src/edk2/Conf
ENV PYTHON_COMMAND=python3
ENV PATH=$PATH:/src/edk2/BaseTools/Bin/Linux-x86_64:/src/edk2/BaseTools/BinWrappers/PosixLike
RUN ./edksetup.sh
RUN BaseTools/BinWrappers/PosixLike/build \
    -p OvmfPkg/OvmfPkgX64.dsc \
    -a X64 \
    -n 8 \
    -b RELEASE \
    -t GCC5 \
    -DSECURE_BOOT_ENABLE=TRUE \
    -DDEBUG_ON_SERIAL_PORT=FALSE \
    -DFD_SIZE_4MB \
    -DFD_SIZE_IN_KB=4096 \
    -DNETWORK_IP6_ENABLE \
    -DNETWORK_TLS_ENABLE \
    -DNETWORK_HTTP_BOOT_ENABLE=TRUE \
    -DTPM_CONFIG_ENABLE \
    -DTPM2_ENABLE \
    -DCC_MEASUREMENT_ENABLE=TRUE

FROM ubuntu:22.04@sha256:0e5e4a57c2499249aafc3b40fcd541e9a456aab7296681a3994d631587203f97

ARG release
ARG branch
ARG nova_uid
ARG nova_gid
ARG libvirt_gid
ARG libvirt_version
ARG DESTDIR

ARG CINDER_GID
ENV CINDER_GID=${CINDER_GID}

ARG PYTHON_BUILD_PACKAGES
ARG QEMU_REQUIRED_PACKAGES

COPY --from=builder ${DESTDIR}/ /
COPY --from=swtpm-builder /build/*.deb /tmp/

COPY patches-nova/*.patch /tmp/
RUN set -eux ; \
    groupadd --gid ${nova_gid} nova; \
    groupadd --gid ${libvirt_gid} libvirt; \
    adduser --disabled-password --gecos "" --uid ${nova_uid} --gid ${nova_gid} nova; \
    usermod -aG libvirt nova; \
    truncate --size=3232 /var/log/faillog; \
    truncate --size=29492 /var/log/lastlog; \
    apt-get update; \
    apt-get install -y --no-install-recommends \
        ${PYTHON_BUILD_PACKAGES} \
        ${QEMU_REQUIRED_PACKAGES} \
        bridge-utils \
        ceph-common \
        cgroup-tools \
        curl \
        dmidecode \
        dosfstools \
        genisoimage \
        iproute2 \
        jq \
        netcat-openbsd \
        nfs-common \
        numad \
        openssh-server \
        openvswitch-switch \
        ovmf \
        python3-pip \
        python3-setuptools \
        qemu-utils \
        patch \
        sudo \
        /tmp/swtpm_* \
        /tmp/swtpm-tools* \
        /tmp/swtpm-libs*;\
    pip3 install --no-cache-dir jc; \
    (git clone https://github.com/openstack/requirements.git --depth 1 --branch stable/${branch} \
     || git clone https://github.com/openstack/requirements.git --depth 1 --branch ${branch}-eol \
     || git clone https://github.com/openstack/requirements.git --depth 1 --branch unmaintained/${branch} ); \
    (git clone https://github.com/openstack/nova.git --depth 1 --branch stable/${branch} \
     || git clone https://github.com/openstack/nova.git --depth 1 --branch ${branch}-eol \
     || git clone https://github.com/openstack/nova.git --depth 1 --branch unmaintained/${branch} ); \
    git apply --directory nova /tmp/0001-Compute-wait-for-ovn-network-at-migration.patch; \
    pip3 install --no-cache-dir -c requirements/upper-constraints.txt PyMySQL python-memcached pymemcache python-binary-memcached nova/; \
    pip3 install --no-cache-dir libvirt-python==${libvirt_version}; \
    echo "oslo.messaging>=$(pip3 show oslo.messaging | grep Version | awk '{print $2}')" > oslo_messaging_constraint.txt ; \
    pip3 install -c oslo_messaging_constraint.txt --upgrade oslo.messaging==14.7.0 || true ; \
    apt-get purge --autoremove -y \
        ${PYTHON_BUILD_PACKAGES}; \
    apt-get clean autoclean; \
    rm -rf /var/lib/{apt,dpkg,cache,log}/; \
    rm -rf /requirements /nova oslo_messaging_constraint.txt; \
    rm -f /tmp/swtpm*; \
    mkdir -m 0700 /etc/ssh/hostkeys/; \
    chown root:root /etc/ssh/hostkeys/; \
    mkdir -p /var/lib/nova; chown nova:nova /var/lib/nova; \
    libvirtd --version; \
    qemu-system-x86_64 --version

COPY files/libvirtd.conf files/qemu.conf /etc/libvirt/
COPY files/*-runner.sh files/*.lib.sh /
COPY files/sshd_config files/ssh_config /etc/ssh/
COPY files/restricted-ssh-commands /usr/lib/restricted-ssh-commands
COPY files/restricted_ssh_commands_nova_zed /etc/restricted-ssh-commands/nova
COPY --chmod=440 files/sudoers /etc/sudoers

# Now copy the build ovmf to our final container
COPY --from=ovmf-builder /src/edk2/Build/OvmfX64/RELEASE_GCC5/FV/OVMF_VARS.fd /usr/share/OVMF/OVMF_VARS_4M.fd
COPY --from=ovmf-builder /src/edk2/Build/OvmfX64/RELEASE_GCC5/FV/OVMF_CODE.fd /usr/share/OVMF/OVMF_CODE_4M.fd

RUN patch /usr/local/lib/python3.10/dist-packages/nova/virt/hardware.py < /tmp/nova-2007697-sev-handle-missing-img-properties.patch
RUN patch /usr/local/lib/python3.10/dist-packages/nova/compute/manager.py < /tmp/7024764.patch
RUN patch /usr/local/lib/python3.10/dist-packages/nova/compute/manager.py < /tmp/nova-delete-volume-attachments-if-vm-was-deleted-during-live-migration.patch
RUN cd /usr/local/lib/python3.10/dist-packages/nova && patch -p2 -i /tmp/nova-23cda5f-increase-num-pcie-ports.patch
RUN cd /usr/local/lib/python3.10/dist-packages/nova && patch -p2 -i /tmp/0001-feat-vm-uuid-in-baseBoard-asset-tag.patch
# The following two patches can only be removed after we upgrade to the next major version of yaook
RUN patch -d /usr/local/lib/python3.*/dist-packages/oslo_messaging -p 2 -i /tmp/recreate-reply-queues-and-keep-existing-exchanges.patch
RUN patch -d /usr/local/lib/python3.*/dist-packages/nova -p 2 -i /tmp/fix-nova-heartbeat-logging-filter.patch
